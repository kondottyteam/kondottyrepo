<?php
 class Committee_model extends CI_Model {
	protected $table='standing_committee';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('standing_committee');
		$this->db->order_by('standing_committee.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	}
	
	public function updateAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "id = ".$id)) { 
            return true; 
         } 
    } 
		
	public function getcomData(){
	    $this->db->select('*');
		$this->db->from('standing_committee');
		$this->db->order_by('id','desc');		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
}