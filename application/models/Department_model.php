<?php
 class Department_model extends CI_Model {
	protected $table='departments';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('departments');
		$this->db->order_by('departments.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
    } 
	public function getDepartment(){
	    $this->db->select('*');
		$this->db->from('departments');
		$this->db->order_by('departments.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
}