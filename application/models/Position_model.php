<?php 
   class Position_model extends CI_Model {
	protected $table='positions';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
     // insert methode
	public function insertData($params){
		$insert = $this->db->insert($this->table,$params);
		return $insert;
	}

	//gett all details from representative table
	public function getAllData($limit,$offset){
		//$this->db->order_by('ID','ASC');
		$this->db->from($this->table);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		return $query->result();
	}

	//getdata for edit
	public function getUpdateData($data){
		//$this->db->select($data['fields']);
		$query	=	$this->db->get_where($this->table,$data['condition']);
		return $query->result_array();
	}

	public function updateAction($params,$editId)
	{
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	
	 public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
      public function getPositionData(){
	    $this->db->select('*');
		$this->db->from('positions');
		$this->db->order_by('id','asc');
		$this->db->limit(1000000,3);		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}	
}