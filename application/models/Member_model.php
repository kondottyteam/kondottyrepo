<?php
 class Member_model extends CI_Model {
	protected $table='member';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('member.id,member.memberName,member.designation,departments.department,member.address,member.phone');
		$this->db->from('member');
		$this->db->join('departments','member.departmentId=departments.id');
		$this->db->order_by('member.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	}
	
	public function updateAction($params,$editId)
	{
		$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
    } 
	public function getMemberData($deptId)
	{ 
		$this->db->select('id');
		$this->db->from('member');
		$this->db->where('departmentId',$deptId);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();		
	}
}