<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Web_model extends MY_Model
{
	protected $ad					=	'ad';
	protected $emergency_contacts	=	'emergency_contacts';
	protected $enquiry				=	'enquiry';
	protected $flash_news			=	'flash_news';
	protected $gallery				=	'gallery';
	protected $hotspot				=	'hotspot';
	protected $map					=	'map';
	protected $medical_equipment	=	'medical_equipment';
	protected $news					=	'news';
	protected $palliative			=	'palliative';
	protected $plan_category		=	'plan_category';
	protected $plan_details			=	'plan_details';
	protected $pmc					=	'pmc';
	protected $positions			=	'positions';
	protected $representatives		=	'representatives';
	protected $rep_position			=	'rep_position';
	protected $services				=	'services';
	protected $service_category		=	'service_category';
	protected $service_office		=	'service_office';
	protected $service_sub_category	=	'service_sub_category';
	protected $standing_committee	=	'standing_committee';
	protected $stand_com_details	=	'stand_com_details';
	protected $statistics			=	'statistics';
	protected $departments			=	'departments';
	protected $member				=	'member';
		
	function fetchAll($key)
	{
		if($key=='ad'){
			$this->db->order_by('ad.id','asc');
			$query = $this->db->get($this->ad);
		}
		elseif($key=='emergency_contacts'){
			$this->db->order_by('emergency_contacts.id','asc');
			$query = $this->db->get($this->emergency_contacts);
		}
		elseif($key=='enquiry'){
			$this->db->order_by('enquiry.id','asc');
			$query = $this->db->get($this->enquiry);
		}
		elseif($key=='flash_news'){
			$this->db->order_by('flash_news.id','desc');
			$query = $this->db->get($this->flash_news);
		}
		elseif($key=='gallery'){
$this->db->select('gallery.id as categoryId,gallery.image as categoryImageUrl,gallery.description as categoryDescription,sub_gallery.id as imageId,sub_gallery.subImage as imageUrl');
			$this->db->from('gallery');
			$this->db->join('sub_gallery', 'sub_gallery.galleryId = gallery.id', 'left');
			$query = $this->db->get(); 
		}
		elseif($key=='hotspot'){
			$this->db->order_by('hotspot.id','asc');
			$query = $this->db->get($this->hotspot);
		}
		elseif($key=='map'){
			$this->db->order_by('map.id','asc');
			$query = $this->db->get($this->map);
		}
		elseif($key=='medical_equipment'){
			$this->db->order_by('medical_equipment.id','asc');
			$query = $this->db->get($this->medical_equipment);
		}
		elseif($key=='news'){
			$this->db->order_by('news.newsDate','desc');
			$query = $this->db->get($this->news);
		}
		if($key=='palliative'){
			$this->db->order_by('palliative.id','asc');
			$query = $this->db->get($this->palliative);
		}
		elseif($key=='plan_category'){
			$this->db->order_by('plan_category.id','asc');
			$query = $this->db->get($this->plan_category);
		}
		elseif($key=='plan_details'){
			$this->db->select('plan_details.id,plan_details.categoryId,plan_details.heading,plan_details.link,plan_category.category');
			$this->db->from('plan_details','plan_category');
			$this->db->join('plan_category','plan_details.categoryId = plan_category.id');
			$this->db->order_by('plan_details.id','asc');
			$query = $this->db->get();
			//echo $this->db->last_query();die;
		}
		elseif($key=='pmc'){
			$this->db->order_by('pmc.id','asc');
			$query = $this->db->get($this->pmc);
		}
		elseif($key=='positions'){
			$this->db->order_by('positions.id','asc');
			$query = $this->db->get($this->positions);
		}
		elseif($key=='representatives'){
			$this->db->order_by('representatives.id','asc');
			$query = $this->db->get($this->representatives);
		}
		elseif($key=='rep_position'){
			$this->db->select('representatives.id as representativeId,
							   representatives.name,
							   representatives.gender,
							   representatives.age,
							   representatives.address,
							   representatives.phone,
							   representatives.mobile,
							   representatives.maritalStatus,
							   representatives.eduQualification,
							   representatives.job,
							   representatives.party,
							   representatives.wardNumber,
							   representatives.wardName,
							   representatives.photo,
							   positions.position');
			$this->db->from('rep_position');
			$this->db->join('representatives','rep_position.representativeId = representatives.ID');
			$this->db->join('positions','rep_position.positionId = positions.ID');
			$this->db->where('rep_position.positionId != ',3,FALSE);
			$this->db->group_by('rep_position.representativeId');
            $this->db->order_by('rep_position.id','asc');
			$query = $this->db->get();
		   //echo $this->db->last_query();die;
		    return $query->result_array();
		}
		elseif($key=='rep_position_staff'){
			$this->db->select('staff.*');
			$this->db->from('staff');
			$this->db->join('rep_position','rep_position.representativeId = staff.id');
			$this->db->join('positions','rep_position.positionId = positions.ID');
			$this->db->where('rep_position.positionId = ',3,FALSE);
			$this->db->group_by('rep_position.representativeId');
            $this->db->order_by('rep_position.id','asc');
			$query = $this->db->get();
		   //echo $this->db->last_query();die;
		    return $query->result_array();
		}
		elseif($key=='services'){
			$this->db->select('service_category.category,services.description,service_office.id,services.link,service_office.office,services.subCategory');
			$this->db->from('services');
			/*$this->db->join('service_sub_category','services.subCategoryId = service_sub_category.id');*/
			$this->db->join('service_category','services.categoryId = service_category.id');
			$this->db->join('service_office','service_category.officeId = service_office.id');
			$this->db->order_by('services.id','asc');
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			 return $query->result_array();
		}		
		elseif($key=='service_category'){
			$this->db->select('service_category.category,service_office.office');
			$this->db->from('service_category');
			$this->db->join('service_office','service_category.officeId = service_office.id');
			$this->db->order_by('service_category.id','asc');
			$query = $this->db->get();
		}
		elseif($key=='service_office'){
			$this->db->order_by('service_office.id','asc');
			$query = $this->db->get($this->service_office);
		}
		elseif($key=='service_sub_category'){
			$this->db->select('service_sub_category.subCategory,service_category.category');
			$this->db->from('service_sub_category');
			$this->db->join('service_category','service_sub_category.categoryId = service_category.id');
			$this->db->order_by('service_sub_category.id','asc');
			$query = $this->db->get();
		}
		elseif($key=='standing_committee'){
			$this->db->order_by('standing_committee.id','asc');
			$query = $this->db->get($this->standing_committee);
		}
		elseif($key=='stand_com_details'){
			$this->db->order_by('stand_com_details.id','asc');
			$query = $this->db->get($this->stand_com_details);
		}
		elseif($key=='statistics'){
			$this->db->order_by('statistics.id','asc');
			$query = $this->db->get($this->statistics);
		}
		elseif($key=='member'){
			$this->db->select('member.id,member.memberName,member.designation,departments.department,departments.id as deptId,member.address,member.phone');
			$this->db->from('member');
			$this->db->join('departments','member.departmentId=departments.id');
			$this->db->order_by('member.memberName','asc');
			$query = $this->db->get();
		}		
        return $query->result_array();		
	}
	function committeeDetails($comId)
	{
		$this->db->select('standing_committee.id as id,
						   standing_committee.standingCommittee,
						   representatives.id as representativeId,
						   representatives.name,
						   representatives.gender,
						   representatives.age,
						   representatives.address,
						   representatives.phone,
						   representatives.mobile,
						   representatives.maritalStatus,
						   representatives.eduQualification,
						   representatives.job,
						   representatives.party,
						   representatives.wardNumber,
						   representatives.wardName,
						   representatives.photo,
						   positions.position');
		$this->db->from('stand_com_details');
		$this->db->join('representatives','stand_com_details.representativeId = representatives.ID');
		$this->db->join('standing_committee','stand_com_details.standingComId = standing_committee.ID');
		$this->db->join('positions','stand_com_details.positionId = positions.ID');
		$this->db->where('stand_com_details.standingComId', $comId);
		$this->db->order_by('stand_com_details.ID','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}
public function insertData($params){ 
		$ins		  =	$this->db->insert($this->enquiry,$params);//echo $this->db->last_query();die;
		return $ins;
	}
}