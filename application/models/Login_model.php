<?php
defined('BASEPATH') or exit('No direct script access allowed');
Class Login_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	public function userlogin($username,$password){
			$this->db->select('password');
			$this->db->from('login');
			$this->db->where('username',$username);
			$hash = $this->db->get()->row('password');
			return $this->verify_password($password,$hash);
	}

	// function for checking the password with the existing password
	public function verify_password($password,$hash)
	{
		return password_verify($password,$hash);
	}

	//get user id from username
	public function get_user_id($username)
	{
		$this->db->select('id');
		$this->db->from('login');
		$this->db->where('username',$username);

		$userid = $this->db->get()->row('id');
		return $userid;
	}

	// get user complete data with userid

	public function get_user($userid)
	{
		$this->db->from('login');
		$this->db->where('id', $userid);

		return $this->db->get()->row();
	}

	/**
     * verify_password_hash function.
     *
     * @access private
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    private function verify_password_hash($password, $hash) {

        return password_verify($password, $hash);

    }
	/**
     * Public method to check password match using the private method verify_password_hash
     *
     * @access public
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    public function checkPasswordMatch ($password, $hash) {
        return $this->verify_password_hash($password, $hash);
    }

     public function updatePassword ($user_id, $password) {
        $data = ["password" => $this->hash_password($password)];
        $this->db->set($data);
        $this->db->where('id', $user_id);
        if ($this->db->update('login')) {
            return true;
        } else {
            return false;
        }
    }
	/**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);

    }



}