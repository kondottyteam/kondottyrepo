<?php
 class CommitteeDetails_model extends CI_Model {
	protected $table='stand_com_details';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	     $this->db->select('*');
		$this->db->from('standing_committee');
		$this->db->order_by('standing_committee.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}	
	
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	
	function committeeDetails($comId)
	{
		$this->db->select('stand_com_details.id as id,
						   standing_committee.standingCommittee,
						   representatives.id as representativeId,
						   representatives.name,						  
						   positions.position');
		$this->db->from('stand_com_details');
		$this->db->join('representatives','stand_com_details.representativeId = representatives.ID');
		$this->db->join('standing_committee','stand_com_details.standingComId = standing_committee.ID');
		$this->db->join('positions','stand_com_details.positionId = positions.ID');
		$this->db->where('stand_com_details.standingComId', $comId);
		$this->db->order_by('stand_com_details.ID','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "id = ".$id)) { 
            return true; 
         } 
    }

    public function getRepCount($id,$standingComId) {
		$this->db->where('representativeId',$id);
		$this->db->where('standingComId',$standingComId);
		$query= $this->db->get('stand_com_details');
	  	$num = $query->num_rows();
	  	return $num;
	}
public function deleteRepData($id) { 
         if ($this->db->delete($this->table, "representativeId = ".$id)) { 
            return true; 
         } 
      } 
 
}