<?php
defined('BASEPATH') OR exit("No direct script access allowed");

class Rep_position_model extends CI_model
{
	protected $table='rep_position';
	protected $table2='positions';
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//gett all details from representative table
	public function getAllData($limit,$offset){
		 $this->db->select('rep_position.*,representatives.*,positions.*,rep_position.ID as rep_pos_id,representatives.ID as rep_id,positions.id as pos_id');
			$this->db->from('rep_position');
			$this->db->join('representatives','rep_position.representativeId = representatives.ID');	
			$this->db->join('positions','rep_position.positionId = positions.ID');	
			$this->db->order_by('rep_position.ID','Asc');
            $this->db->limit($limit, $offset);
			$query = $this->db->get();
			return $query->result();
	}

	// insert methode
	public function insertData($params){
		$insert = $this->db->insert($this->table,$params);
		return $insert;
	}

	//for getting data from the defined table
	public function getDataFrom($table)
	{
		if($table==$this->table2){
			$this->db->limit('2');
		}
		$query = $this->db->get($table);
		return $query->result();
	}

	//check whether the position is already allocated or not
	public function checkExisting($positionid){
		//$this->db->where('positionId',$positionid);
		$query = $this->db->get_where($this->table, array('positionId' =>$positionid));
		return $query->num_rows();
	}

	//updating position 
	public function updateData($params,$position){
		$this->db->where('positionId',$position);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function getSecretary(){
		 $this->db->select('staff.*');
			$this->db->from('staff');
			$query = $this->db->get();
			return $query->result();
	}
	public function insertStaffData($params){
		$insert = $this->db->insert('staff',$params);
		return $insert;
	}
	public function insertStaffPosition($params){
		$insert = $this->db->insert('rep_position',$params);
		return $insert;
	}
	public function updateStaffData($params,$position){
		$this->db->where('id',$position);
		$up		=	$this->db->update('staff',$params);	
		return $up;
	}
	public function updatePosMapping($params,$position){
		$this->db->where('positionId',$position);
		$up		=	$this->db->update('rep_position',$params);	
		return $up;
	}
	public function getSecrtary(){
		 $this->db->select('staff.*');
			$this->db->from('rep_position');
			$this->db->join('staff','rep_position.representativeId = staff.id');	
			$this->db->join('positions','rep_position.positionId = positions.ID');	
			$this->db->order_by('rep_position.ID','Asc');
			$query = $this->db->get();
			//echo $this->db->last_query();die;
			return $query->result();
	}
	public function deleteRepData($id) { 
         if ($this->db->delete($this->table, "representativeId = ".$id)) { 
            return true; 
         } 
      }
    public function getStaff($position){
        $this->db->select('representatives.id,representatives.name');
        $this->db->from('representatives');
        $this->db->join('rep_position','rep_position.representativeId = representatives.ID');
        $this->db->where('rep_position.id',$position);
        $query = $this->db->get();
        return $query->result();
    }
}