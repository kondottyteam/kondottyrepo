<?php
 class Enquiry_model extends CI_Model {
	protected $table='enquiry';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('enquiry');
		$this->db->order_by('enquiry.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
    } 
	
}