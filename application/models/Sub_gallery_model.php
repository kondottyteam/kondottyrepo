<?php
 class Sub_gallery_model extends CI_Model {
	protected $table='sub_gallery';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
	
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "id = ".$id)) { 
            return true; 
         } 
    } 
        public function deleteSubData($id) { 
    	if ($this->db->delete($this->table, "galleryId = ".$id)) { 
            return true; 
         } 
    } 
    public function getCount($galleryId){
	 $this->db->select('count(*) as imgCount');
		$this->db->from('sub_gallery');		
		$this->db->where('galleryId', $galleryId);
		$imgCount = $this->db->get()->row('imgCount');
		//echo $this->db->last_query();die;
		//echo $imgCount;die;
		return $imgCount;
	}
	public function getSubCount(){
	    $this->db->select('count(subImage) as imgCount');
		$this->db->from('sub_gallery');		
		$imgCount = $this->db->get()->row('imgCount');
		//echo $this->db->last_query();die;
		//echo $imgCount;die;
		return $imgCount;
	}

       //for removing sub gallery image while removing main gallery image

       public function rowWiseData($editId)
	{
		$query 	= $this->db->query("SELECT subImage FROM sub_gallery where galleryId='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	} 

        //for removing the subgallery image when deleting the image from sub gallery

        public function rowWiseData2($editId)
	{
		$query 	= $this->db->query("SELECT subImage FROM sub_gallery where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	}    


}