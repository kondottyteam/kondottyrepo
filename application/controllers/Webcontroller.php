<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Webcontroller extends REST_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('Web_model');
		$this->load->helper('url');			
	}
	
	function ad_get()
	{
		$results=$this->Web_model->fetchAll('ad');
		if($results)
		{
            $i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('ad'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function emergencyContacts_get()
	{
		$results=$this->Web_model->fetchAll('emergency_contacts');
		if($results)
		{
			$this->response(array('emergencyContacts'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function enquiry_get()
	{
		$results=$this->Web_model->fetchAll('enquiry');
		if($results)
		{
			$this->response(array('enquiries'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function postEnquiry_post()
	{
/*$headers = apache_request_headers();

		foreach ($headers as $header => $value)
		{
		    echo "$header: $value <br />\n";
		 
		}*/
	//{"name":"asnad","mobileNumber":"9746410718",,"enquiryDescription":"description"," aadhaarNumber":"12345"}	
		$params = json_decode(file_get_contents('php://input'), TRUE);
		//print_r($params);
		$eDate=date('Y-m-d');
        $user = array(
            'userName'  	=> $params['name'],
            'phone' 		=> $params['mobileNumber'],
            'description' 	=> $params['enquiryDescription'],
            'aadhar' 		=> $params['aadhaarNumber'],
            'address' 		=> $params['address'],
            'eDate'		=> $eDate	
        ); 
        //print_r($user);
        $result = $this->Web_model->insertData($user);
        if ($result) {
            //$this->response($result, 200); // 200 being the HTTP response code
            $this->response(array('status'=>'Success','message'=>'Thank you for your feedback. We will get back you soon'),200);
        } else {
            $this->response(NULL, 404);
               }
	} 
        function scroll_get()
	{
		$results=$this->Web_model->fetchAll('flash_news');
		if($results)
		{
			$i = 0;$scrollNews='';
			$count=count($results);
			foreach($results as $r)
			{
				$scrollNews 	= $scrollNews . $r['news'];
				$i += 1;
				if($count!=$i)
				$scrollNews		=	$scrollNews." - ";
			}
			$this->response(array('scrollNews'=>$scrollNews,'status'=>'Success','message'=>'News loaded successfully'));
		}
		
	}
	function gallery_get()
	{
		$results=$this->Web_model->fetchAll('gallery');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results2[$i]['categoryId'] 		 = $r['categoryId'];
				$results2[$i]['categoryImageUrl']    = base_url().$r['categoryImageUrl'];
				$results2[$i]['categoryDescription'] = $r['categoryDescription'];
				$results2[$i]['imageId'] 		 	 = $r['imageId'];
				$results2[$i]['imageUrl']    		 = base_url().$r['imageUrl'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('galleryImages'=>$results2,'status'=>'Success','message'=>'Data loaded successfully'));
		}	
	}
	function hotspot_get()
	{
		$results=$this->Web_model->fetchAll('hotspot');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				unset($results[$i]['photo']);
				unset($results[$i]['photo2']);
				unset($results[$i]['photo3']);
				/*$pics = array (
				array("imageId" => "1", "imageUrl" => base_url().$r['photo']),
				array("imageId" => "1", "imageUrl" => base_url().$r['photo2']),
				array("imageId" => "1", "imageUrl" => base_url().$r['photo3'])
				);*/

				//$results[$i]['photo'] = base_url().$r['photo'];
				//$results[$i]['photo2'] = base_url().$r['photo2'];
				//$results[$i]['photo3'] = base_url().$r['photo3'];

                               $pics=array();
                $pic=array();
				if($r['photo']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo'];
					array_push($pics,$pic);
				}
				if($r['photo2']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo2'];
					array_push($pics,$pic);
				}
				if($r['photo3']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo3'];
					array_push($pics,$pic);
				}
				$results[$i]['images'] = $pics;
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('hotspots'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
			
		}
		
	}
	function maps_get()
	{
		$results=$this->Web_model->fetchAll('map');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('maps'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function medical_equipment_get()
	{
		$results=$this->Web_model->fetchAll('medical_equipment');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('medical_equipment'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function news_get()
	{
		$results=$this->Web_model->fetchAll('news');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$newsDate				=	$r['newsDate'];
				$frontendDate 				= 	date('d-M-Y', strtotime($newsDate));
				
				$results2[$i]['id'] 	 		= 	$r['id'];
				$results2[$i]['heading'] 		= 	$r['heading'];
				$results2[$i]['description'] 	        = 	$r['description'];
				$results2[$i]['date'] 	 		= 	$frontendDate;
				$results2[$i]['imageUrl'] 		= 	base_url().$r['imageUrl'];
				$i += 1;
			}
			$this->response(array('news'=>$results2,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}	
	function palliative_get()
	{
		$results=$this->Web_model->fetchAll('palliative');
		if($results)
		{
			$this->response(array('palliative'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function plan_category_get()
	{
		$results=$this->Web_model->fetchAll('plan_category');
		if($results)
		{
			$this->response(array('plan_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function plans_get()
	{
		$results=$this->Web_model->fetchAll('plan_details');
		if($results)
		{
			$this->response(array('plans'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function pmc_get()
	{
		$results=$this->Web_model->fetchAll('pmc');
		if($results)
		{
			$this->response(array('pmc'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function positions_get()
	{
		$results=$this->Web_model->fetchAll('positions');
		if($results)
		{
			$this->response(array('positions'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function representatives_get()
	{
		$results=$this->Web_model->fetchAll('representatives');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('representatives'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function rep_position_get()
	{
		$results=$this->Web_model->fetchAll('rep_position');
		if($results)
		{
			$this->response(array('rep_position'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
    function services_get()
	{
		$results=$this->Web_model->fetchAll('services');//print_r($results);
		if($results){	
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['heading'] = $r['subCategory'];
				$i += 1;
			}	
			$this->response(array('services'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
	
	}
	function service_category_get()
	{
		$results=$this->Web_model->fetchAll('service_category');
		if($results)
		{
			$this->response(array('service_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function service_office_get()
	{
		$results=$this->Web_model->fetchAll('service_office');
		if($results)
		{
			$this->response(array('service_office'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function service_sub_category_get()
	{
		$results=$this->Web_model->fetchAll('service_sub_category');
		if($results)
		{
			$this->response(array('service_sub_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}function committee_get()
	{
		$results=$this->Web_model->fetchAll('standing_committee');
		if($results)
		{
			$this->response(array('committee'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function standingCommittee_get()
	{
		$respo = array();$i=$j=0;
		$results1=$this->Web_model->fetchAll('rep_position');
		foreach($results1 as $r1)
		{	
			$respo[$j]['id'] 				  = 0;
			$respo[$j]['standingCommittee']   = "Municipality";
			$respo[$j]['representativeId']    = $r1['representativeId'];
			$respo[$j]['name']  			  = $r1['name'];
			$respo[$j]['gender']  		  	  = $r1['gender'];
			$respo[$j]['age']  			      = $r1['age'];
			$respo[$j]['address']  		      = $r1['address'];
			$respo[$j]['phone']  			  = $r1['phone'];
			$respo[$j]['mobile']  		      = $r1['mobile'];
			$respo[$j]['maritalStatus']  	  = $r1['maritalStatus'];
			$respo[$j]['eduQualification']    = $r1['eduQualification'];
			$respo[$j]['job']  			      = $r1['job'];
			$respo[$j]['party']  			  = $r1['party'];
			$respo[$j]['wardNumber']  	      = $r1['wardNumber'];
			$respo[$j]['wardName']  		  = $r1['wardName'];
			$respo[$j]['photo']  			  = base_url().$r1['photo'];
			$respo[$j]['position']  		  = $r1['position'];	
			$j++;				
		}
		//secretary
		$results4=$this->Web_model->fetchAll('rep_position_staff');//var_dump($results4);die;
		foreach($results4 as $r4)
		{	
			$respo[$j]['id'] 				  = 0;
			$respo[$j]['standingCommittee']   = "Municipality";
			$respo[$j]['representativeId']    = $r4['id'];
			$respo[$j]['name']  			  = $r4['staffName'];
			$respo[$j]['gender']  		  	  = 0;
			$respo[$j]['age']  			      = 0;
			$respo[$j]['address']  		      = $r4['address'];
			$respo[$j]['phone']  			  = $r4['phone'];
			$respo[$j]['mobile']  		      = 0;
			$respo[$j]['maritalStatus']  	  = 0;
			$respo[$j]['eduQualification']    = 0;
			$respo[$j]['job']  			      = 0;
			$respo[$j]['party']  			  = 0;
			$respo[$j]['wardNumber']  	      = 0;
			$respo[$j]['wardName']  		  = 0;
			$respo[$j]['photo']  			  = base_url().$r4['photo'];
			$respo[$j]['position']  		  = $r4['designation'];	
			$j++;				
		}
		$results2=$this->Web_model->fetchAll('standing_committee');
		foreach($results2 as $r2)
		{			
			$id						   =	$r2['id'];			
			$results3  				   =	$this->Web_model->committeeDetails($id);
			//if($results2)
			//array_push($respo, $results2);			
			foreach($results3 as $r3)
			{		
				
				$respo[$j]['id'] 				  = $r3['id'];
				$respo[$j]['standingCommittee']   = $r3['standingCommittee'];
				$respo[$j]['representativeId']    = $r3['representativeId'];
				$respo[$j]['name']  			  = $r3['name'];
				$respo[$j]['gender']  		      = $r3['gender'];
				$respo[$j]['age']  			      = $r3['age'];
				$respo[$j]['address']  		      = $r3['address'];
				$respo[$j]['phone']  			  = $r3['phone'];
				$respo[$j]['mobile']  		      = $r3['mobile'];
				$respo[$j]['maritalStatus']  	  = $r3['maritalStatus'];
				$respo[$j]['eduQualification']    = $r3['eduQualification'];
				$respo[$j]['job']  			      = $r3['job'];
				$respo[$j]['party']  			  = $r3['party'];
				$respo[$j]['wardNumber']  	      = $r3['wardNumber'];
				$respo[$j]['wardName']  		  = $r3['wardName'];
				$respo[$j]['photo']  			  = base_url().$r3['photo'];
				$respo[$j]['position']  		  = $r3['position'];		
				$j++;				
			}
		}
		$this->response(array('committees'=>$respo,'status'=>'Success','message'=>'Data loaded successfully'));	
	}
	function statistics_get()
	{
		$results=$this->Web_model->fetchAll('statistics');
		if($results)
		{
			$this->response(array('statistics'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function member_get()
	{
		$results=$this->Web_model->fetchAll('member');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results2[$i]['memberId'] 	 	= 	$r['id'];
				$results2[$i]['name'] 			= 	$r['memberName'];
				$results2[$i]['designation'] 	= 	$r['designation'];
				$results2[$i]['address'] 	 	= 	$r['address'];
				$results2[$i]['phoneNumber'] 	= 	$r['phone'];
				$results2[$i]['department'] 	= 	$r['department'];
				$results2[$i]['departmentId'] 	= 	$r['deptId'];
				$i += 1;
			}
			$this->response(array('departments'=>$results2,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}	
}	