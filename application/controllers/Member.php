<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	protected $baseFolder		=	'admin/member';
	protected $table			=	'member';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Member_model'));
        $this->load->model(array('Department_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Member/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Member_model->getAllData($config['per_page'],$this->uri->segment(3));        
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
     	$data['dept'] = $this->Department_model->getDepartment();
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$memberName				= NULL;
	 	$departmentId			= NULL;
	 	$designation			= NULL;
	 	$address				= NULL;
	 	$phone					= NULL;
     	$submit 				= NULL;
     	
     	extract($_POST);
     	
     	$params['memberName']			=	$memberName; 
     	$params['departmentId']			=	$departmentId;
     	$params['designation']			=	$designation;
     	$params['address']				=	$address;
     	$params['phone']				=	$phone; 
     	
     	if(isset($submit))
     	{		
			$res=$this->Member_model->insertData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Member/index');		
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'id',
		'memberName',
		'departmentId',
		'designation',
		'address',
		'phone'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Member_model->getUpdateData($data);
         $data['dept'] = $this->Department_model->getDepartment();
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$memberName		= NULL;
	 	$departmentId	= NULL;
	 	$designation	= NULL;
	 	$address		= NULL;
	 	$phone			= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	
     	$params['memberName']			=	$memberName; 
     	$params['departmentId']			=	$departmentId;
     	$params['designation']			=	$designation;
     	$params['address']				=	$address;
     	$params['phone']				=	$phone; 
     	if(isset($submit))
     	{			
			$res=$this->Member_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Member/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Member_model->deleteData($id);  
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Member/index');  		
      }
}
