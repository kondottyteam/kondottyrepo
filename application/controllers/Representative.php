<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Representative extends CI_Controller
{
	protected $table = 'representatives';
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->model('Representative_model');
		$this->load->model('CommitteeDetails_model');
		$this->load->model('Rep_position_model');
		$this->load->library('upload');
                $this->load->library('session');
		if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
	}

	public function index(){
		if(@$_SESSION['logged_in'] && @$_SESSION['user_type']=='admin')
		{
			$this->load->library('pagination');
			$num_rows = $this->db->count_all("$this->table");

			$config['base_url'] = base_url().'index.php/representative/index';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = 15;
			
			//$config['use_page_numbers'] = TRUE;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);

			$data['records']  = $this->Representative_model->getAllData($config['per_page'],$this->uri->segment(3));
			$this->load->view('admin/header');
			$this->load->view('admin/representatives/index',$data);
			$this->load->view('admin/footer');
		}
	}

	public function add(){
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('mobile','Mobile','required');
		$this->form_validation->set_rules('wardno','Ward number','required');

		if($this->form_validation->run()==false){
			$this->load->view('admin/header');
			$this->load->view('admin/representatives/add.php');
			$this->load->view('admin/footer');
		}
		else{

			$name 			= Null;
			$gender 		= Null;
			$genderMal 		= Null;
			$age			= Null;
			$address 		= Null;
			$phone 			= Null;
			$mobile 		= Null;
			$marital 		= Null;
			$maritalMal		= Null;
			$qualification	= Null;
			$job			= Null;
			$party			= Null;
			$wardno			= Null;
			$wardname		= Null;
			$submit 		= Null;
			$position 		= Null;

			extract($_POST);

			//gender
			if($gender=='M'){
				$genderMal = 'പുരുഷൻ';
			}elseif($gender=='F'){
				$genderMal = 'സ്ത്രീ';
			}

			//marital status
			if($gender=='M'){
				if($marital=='N'){
					$maritalMal = 'അവിവാഹിതൻ';
				}
				elseif($marital=='Y'){
					$maritalMal = 'വിവാഹിതൻ';
				}
				elseif($marital=='W'){
					$maritalMal = 'വിഭാര്യൻ';
				}
			}
			if($gender=='F'){
				if($marital=='N'){
					$maritalMal = 'അവിവാഹിത';
				}
				elseif($marital=='Y'){
					$maritalMal = 'വിവാഹിത';
				}
				elseif($marital=='W'){
					$maritalMal = 'വിധവ';
				}
			}

			$params['name'] 			= $name;
			$params['gender'] 			= $genderMal;
			$params['genderEng']		= $gender;
			$params['age'] 				= $age;
			$params['address'] 			= $address;
			$params['phone'] 			= $phone;
			$params['mobile'] 			= $mobile;
			$params['maritalStatus'] 	= $maritalMal;
			$params['maritalStatusEng'] = $marital;
			$params['eduQualification'] = $qualification;
			$params['job'] 				= $job;
			$params['party'] 			= $party;
			$params['wardNumber'] 		= $wardno;
			$params['wardName'] 		= $wardname;
					
			$params['position'] 		= $position;

                        //ward no is alredy exists validation
     		$wardnoCount = $this->Representative_model->wardno_exist($wardno);//echo $wardnoCount;die;
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $rep_image=array(
            	'upload_path'=>'./images/rep/',
            	'allowed_types'=>'jpg|jpeg|png',
            	'file_name' =>$wardno
            );
          $this->upload->initialize($rep_image);
          if($this->upload->do_upload('photo')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['photo']  	=	"images/rep/".$fileUpload['file_name'];
		  }
		  if(!$this->upload->do_upload('photo'))
	     	 {
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please upload 'jpg | jpeg | png' type files.."]);
			 	 redirect('representatives/add');
			 }
     	 // echo $fileUpload['file_name'];die; 
            if($wardnoCount>0){
				$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Ward Number Already Exists.!"]);
			redirect('representatives/add');
		}
     	
     	else {                    

			if (isset($submit)) {
				$result = $this->Representative_model->insertData($params);

			}
			if($result){
				$this->session->set_flashdata("flash",["type"=>"success","message"=>"Data added successfully"]);
			}
			else{
				$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Failed to add data!"]);
			}
			redirect('representatives/index');
                       }
		}
	}

	public function edit_view(){
		$data['fields']=array(
		'id',
		'name',
		'age',
		'gender',
		'genderEng',
		'address',
		'phone',
		'mobile',
		'maritalStatus',
		'maritalStatusEng',
		'eduQualification',
		'job',
		'party',
		'wardNumber',
		'wardName',
		'position',
		'photo'
		);
	 	
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Representative_model->getUpdateData($data);

         $this->load->view('admin/header');
		 $this->load->view('admin/representatives/edit',$data);
		 $this->load->view('admin/footer');
	}

	public function update()
	{
		$editId			= Null;
		$name 			= Null;
		$gender 		= Null;
		$genderMal 		= Null;
		$age			= Null;
		$address 		= Null;
		$phone 			= Null;
		$mobile 		= Null;
		$marital 		= Null;
		$qualification	= Null;
		$job			= Null;
		$party			= Null;
		$wardno			= Null;
		$wardname		= Null;
		$submit 		= Null;
		$changeImage    = Null;
		$position 		= Null;
                $changeImage	= NULL;


     	
     	extract($_POST);
     	$editId				 			=	$editId;
     	if($gender=='M'){
			$genderMal = 'പുരുഷൻ';
		}elseif($gender=='F'){
			$genderMal = 'സ്ത്രീ';
		}

		//marital status
			if($gender=='M'){
				if($marital=='N'){
					$maritalMal = 'അവിവാഹിതൻ';
				}
				elseif($marital=='Y'){
					$maritalMal = 'വിവാഹിതൻ';
				}
				elseif($marital=='W'){
					$maritalMal = 'വിഭാര്യൻ';
				}
			}
			if($gender=='F'){
				if($marital=='N'){
					$maritalMal = 'അവിവാഹിത';
				}
				elseif($marital=='Y'){
					$maritalMal = 'വിവാഹിത';
				}
				elseif($marital=='W'){
					$maritalMal = 'വിധവ';
				}
			}

		$params['name'] 			= $name;
		$params['gender'] 			= $genderMal;
		$params['genderEng']		= $gender;
		$params['age'] 				= $age;
		$params['address'] 			= $address;
		$params['phone'] 			= $phone;
		$params['mobile'] 			= $mobile;
		$params['maritalStatus'] 	= $maritalMal;
		$params['maritalStatusEng'] = $marital;
		$params['eduQualification'] = $qualification;
		$params['job'] 				= $job;
		$params['party'] 			= $party;
		$params['wardNumber'] 		= $wardno;
		$params['wardName'] 		= $wardname;
				
		$params['position'] 		= $position; 
     	if($changeImage=='yes')
     	{     		
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $rep_image=array(
            	'upload_path'=>'./images/rep/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'file_name' =>$wardno
            );
          $this->upload->initialize($rep_image);
          if($this->upload->do_upload('photo')){
          	$res=$this->Representative_model->rowWiseData($editId);
     		if (isset($res))
			{
		        $img= $res->photo;
				if (file_exists($img)) {
		        unlink($img);
		       }
			}
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['photo']  	=	"images/rep/".$fileUpload['file_name'];
		  } 		  
     	 // echo $fileUpload['file_name'];die; 
     	}
     	if($changeImage=='yes')
     	{
	     	if(!$this->upload->do_upload('photo'))
		     {
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please upload 'jpg | jpeg | png ' type files.."]);
			 }
			 else 
	     	{			
				$res=$this->Representative_model->updateAction($params,$editId);
				 if($res)
		         {
				 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
				 }
				 else{
				 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
				 }
			}
		}
     	else if($changeImage=='no')
     	{	
	     	$res=$this->Representative_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('representatives/index');
	}

	public function delete(){
	    $id = $this->uri->segment('3');
        $res2=$this->Representative_model->rowWiseData($id);
 		if (isset($res2))
		{
		    $img= $res2->photo;   
			if (file_exists($img)) {
	        unlink($img);
	       }
		} 
		$res3=$this->CommitteeDetails_model->deleteRepData($id); 
		$res4=$this->Rep_position_model->deleteRepData($id); 
     	$res=$this->Representative_model->deleteData($id); 
     if($res)
     {
	 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
	 }
	 else{
	 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
	 }
    redirect('representatives/index'); 
	}
}