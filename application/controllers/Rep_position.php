<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Rep_position extends CI_Controller
{
	protected $baseFolder		=	'admin/rep_position';
	protected $table1			=	'rep_position';
	protected $table2			=	'positions';
	protected $table3			=	'representatives';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('Rep_position_model');
$this->load->library('session');
$this->load->library('upload');
                if(empty($this->session->userdata("userid")))
                  {
        	   $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	   redirect(site_url(),'refresh');
                  }
	}

	function index(){
		if(@$_SESSION['logged_in']){
			$num_rows=$this->db->count_all("$this->table1");
	     	$this->load->library('pagination');

			$config['base_url'] = base_url().'index.php/position/index';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = 15;
			
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";
			
			$this->pagination->initialize($config);

	        $data['records'] = $this->Rep_position_model->getAllData($config['per_page'],$this->uri->segment(3));
	        $data['positions'] = $this->Rep_position_model->getDataFrom($this->table2);
			$data['representatives']		 = $this->Rep_position_model->getDataFrom($this->table3);
			$data['secretary'] = $this->Rep_position_model->getSecrtary();
			$data['presidentName'] = $this->Rep_position_model->getStaff('1');
			$data['vcName'] = $this->Rep_position_model->getStaff('2');
			$data['president'] = $this->Rep_position_model->checkExisting('1');
			$data['vpresident'] = $this->Rep_position_model->checkExisting('2');
			//echo $data['vpresident'];die;
	        $this->load->view("$this->header");
	        $this->load->view("$this->baseFolder/index",$data);
	        $this->load->view("$this->footer");
        } else {
            redirect(base_url().'index.php/login');
        }
	}

	public function add(){
		$name1 		= null;
		$position1 	= 1;

		$name2 		= null;
		$position2 	= 2;

		extract($_POST);
		
		if(isset($submitPresident)){
			$params1['representativeId'] = $name1;
			$params1['positionId']		= $position1;
			$exist  = $this->Rep_position_model->checkExisting($position1);
			if($exist>0){
				$result = $this->Rep_position_model->updateData($params1,$position1);
			}
			else{
				$result = $this->Rep_position_model->insertData($params1);
			}
			
		}
		if(isset($submitVc)){
			$params2['representativeId'] = $name2;
			$params2['positionId']		= $position2;

			$exist  = $this->Rep_position_model->checkExisting($position2);
			if($exist>0){
				$result = $this->Rep_position_model->updateData($params2,$position2);
			}
			else{
				$result = $this->Rep_position_model->insertData($params2);
			}

		}	
		
		if($result){
			$this->session->set_flashdata("flash",["type"=>"success","message"=>"Data inserted successfully!"]);
		}
		else{
			$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Failed to insert data!"]);
		}
		redirect('rep_position/index');
		
	}
	public function staffAdd(){
		$this->form_validation->set_rules('staffName','Staff Name','required');
		$this->form_validation->set_rules('phone','Phone','required');

		if($this->form_validation->run()==false){
			redirect('rep_position/index');
		}
		else{

			$staffName 		= Null;
			$designation 	= Null;
			$phone 			= Null;			
			$submit 		= Null;			

			extract($_POST);

			$params['staffName'] 			= $staffName;
			$params['designation'] 			= 'secretary';
			$params['phone']				= $phone;
			$params['address'] 				= $address;
			
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $rep_image=array(
            	'upload_path'=>'./images/staff/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            ); 
          $this->upload->initialize($rep_image);
          if($this->upload->do_upload('photo')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['photo']  	=	"images/staff/".$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 $params2['representativeId']  	=	"1";
		 $params2['positionId']  		=	"3";
		//secretary alredy exists validation
     		$secretary['result'] = $this->Rep_position_model->getSecretary();//echo $wardnoCount;die;
			if(count($secretary['result'])>0){
				foreach($secretary['result'] as $res)
				{
					 $img= $res->photo;
				}
			        if (file_exists($img)) {
			        	unlink($img);
			        	//echo $imageName;die;
			        }				
				$result1 = $this->Rep_position_model->updateStaffData($params,1);			
				$result2 = $this->Rep_position_model->updatePosMapping($params2,3);
			}
			else{
				$result1 = $this->Rep_position_model->insertStaffData($params);
				$result2 = $this->Rep_position_model->insertStaffPosition($params2);
			}
      
			if($result1){
				$this->session->set_flashdata("flash",["type"=>"success","message"=>"Data added successfully"]);
			}
			else{
				$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Failed to add data!"]);
			}
			redirect('rep_position/index');       
		}
	}
}