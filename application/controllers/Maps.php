<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maps extends CI_Controller {
	protected $baseFolder		=	'admin/map';
	protected $table			=	'map';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Maps_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Maps/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Maps_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {  
	 	$heading		= NULL;    	
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['heading']		=	$heading;
     	
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./images/map/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($up_image);
          if($this->upload->do_upload('photo')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['photo']  	=	"images/map/".$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 //print_r($params);die;
     	if(!$this->upload->do_upload('photo'))
     	 {
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please upload 'jpg | jpeg | png | gif' type files.."]);
		 }
     	else 
     	{			
			$res=$this->Maps_model->insertData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Maps/index');		
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'id',
		'heading'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Maps_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 	 	
     	$editId			= NULL; 
     	$heading		= NULL;     	
     	$submit 		= NULL;
        $changeImage	= NULL;
     	
     	extract($_POST);
     	$editId				 	=	$editId;
     	$params['heading']		=	$heading; 
     	if($changeImage=='yes')
     	{
     		$res=$this->Maps_model->rowWiseData($editId);
     		if (isset($res))
			{
			        $img= $res->photo;
			        
if (file_exists($img)) {
        unlink($img);
       }
			}
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./images/map',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('photo')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		    } 
     	 // echo $fileUpload['file_name'];die; 
     	 $params['photo']  	=	"images/map/".$fileUpload['file_name'];
		}
     	if(!$this->upload->do_upload('photo'))
     	 {
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Please upload 'jpg | jpeg | png | gif' type files.."]);
		 }
     	else 
     	{				
			$res=$this->Maps_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Maps/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
          $res2=$this->Maps_model->rowWiseData($id);
     		if (isset($res2))
			{
			        $img= $res2->photo;
			        
if (file_exists($img)) {
        unlink($img);
       }
			} 
         $res=$this->Maps_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Maps/index');  		
      }
}
