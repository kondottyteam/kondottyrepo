<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommitteeDetails extends CI_Controller {
	protected $baseFolder		=	'admin/committee_details';
	protected $table			=	'stand_com_details';
        protected $table1			=	'standing_committee';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct(); 		
    	$this->load->model(array('CommitteeDetails_model','Committee_model','Representative_model','Position_model')); 
    	//$this->load->model('Committee_model');
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table1");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/CommitteeDetails/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Committee_model->getAllData($config['per_page'],$this->uri->segment(3));
        $data['recordsAll'] = $this->CommitteeDetails_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
     	$data=array();
     	$data['com']=$this->Committee_model->getcomData();
     	$data['rep']=$this->Representative_model->getRepData();
     	$data['pos']=$this->Position_model->getPositionData();
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$representativeId		= 	NULL;
	 	$standingComId			=	NULL;
	 	$positionId     	   	=	NULL;
     	$submit 				= 	NULL;
     	
     	extract($_POST);
     	$params['standingComId']		=	$standingComId;
     	$params['representativeId']		=	$representativeId;
     	$params['positionId']			=	$positionId;
     	
     	
     	//checking representative is there or not
     	$repCount = $this->CommitteeDetails_model->getRepCount($representativeId,$standingComId);
     	
     	if($repCount>0){
			$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Representative is already assaigned for this Committee.!"]);
			redirect('CommitteeDetails/add_view');
		}
     	
     	else {
			
     	if(isset($submit))
     	{		
			$res=$this->CommitteeDetails_model->insertData($params);			
			 if($res)
	         {	         	
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('CommitteeDetails/index');
		
		}  	
	 }

  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->CommitteeDetails_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('CommitteeDetails/index');  		
      }
      
      function view()
	  {
	  	header('Content-type: application/json');
		$comId		= $this->uri->segment('3');
		$respo  	= array();$i=$j=0;		
				
			$results2  				   		=	$this->CommitteeDetails_model->committeeDetails($comId);
			if($results2)
			//array_push($respo, $results2);			
			foreach($results2 as $r)
			{	
				$respo[$j]['id'] 				  = $r['id'];
				$respo[$j]['standingCommittee']   = $r['standingCommittee'];
				$respo[$j]['representativeId']    = $r['representativeId'];
				$respo[$j]['name']  			  = $r['name'];				
				$respo[$j]['position']  		  = $r['position'];		
				$j++;				
			}
		// print_r($respo);
		//$this->response(array('committees'=>$respo));	
		echo json_encode($respo);
	  }
	  
}
