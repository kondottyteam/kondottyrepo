<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Emergency Contacts
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Emergency Contacts</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">                                
                                   <form action="<?= site_url(); ?>/EmergencyContacts/edit" method="post" >
                                    <?php foreach($results as $r){  ?>
                    			    <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="name">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required value="<?php echo $r['name'];?>">
                                    </div>
                                    <div class="form-group" style="display: none;">
                                        <label for="address">Address</label>
                                        <textarea name="address" id="address" class="form-control" ><?php echo $r['address'];?></textarea>
                                    </div>
                                    <!--   ^\d{5}([- ]*)\d{6}$
                                     will support the following formats:
								    03595-259506
									03592 245902
									03598245785  -->
                                    <div class="form-group">
                                        <label for="phone">Land Phone</label>
                                        <input type="tel" pattern="^\d{4}([- ]*)\d{7}$" name="phone" id="phone" class="form-control" value="<?php echo $r['phone'];?>" placeholder="04xx2xxxxxx">
                                    </div>
                                     <!--   ^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
                                     will support the following formats:
								    8880344456/+918880344456/+91 8880344456/
								    +91-8880344456/08880344456/918880344456  -->
                                     <div class="form-group">
                                        <label for="mobile">Mobile</label><span class="text-danger">*</span>
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" name="mobile" id="mobile" class="form-control" required="" value="<?php echo $r['mobile'];?>" placeholder="+91xxxxxxxxxx">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('bannerImage').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('bannerImage').style.display='none';
	}
}
</script>
