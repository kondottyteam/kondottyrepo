<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Service
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Services</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <form action="<?= site_url(); ?>/Service/service_add" method="post">
                                    <div class="form-group">
                                       <label for="title">Category</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="categoryId" required>
	                                	<option value="">Select</option>
	                                	<?php foreach($category as $cat){ ?>
	                                	<option value="<?= $cat->id; ?>"><?= $cat->category;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Sub Category</label>
                                        <input type="text" name="subCategory" id="subCategory" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label>
                                        <textarea name="description" id="description" class="form-control"  rows="5"></textarea> 
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Link</label>
                                        <input type="text" name="link" id="link" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
