<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Position
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update Position</h3>
                    </div>
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/position/edit" method="post" enctype="multipart/form-data">
                        <div class="row">
                        <?php foreach($results as $posdata){  ?>
                         <input type="hidden" name="editId" value="<?php echo $posdata['id'];?>">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <div class="form-group">
                                    <label for="position">Name</label><span class="text-danger">*</span>
                                    <input type="text" name="position" id="position" class="form-control" required value="<?php echo $posdata['position'];?>">
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                                    </div>
                            </div>
                           
                        </div>
                       <?php } ?>
                    </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

