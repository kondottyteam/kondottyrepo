<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Member
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Member</h3>
                    </div>
                    <div class="box-body">                                                
                        <form action="<?= site_url(); ?>/Member/add" method="post">
                            <div class="row"> 
                            	<div class="col-lg-4 col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label for="memberName">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="memberName" id="memberName" class="form-control"  required>
                                    </div> 
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">     
                                    <div class="form-group">
                                       <label for="title">Department</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="departmentId" required>
	                                	<option value="">Select</option>
	                                	<?php foreach($dept as $d){ ?>
	                                	<option value="<?= $d->id; ?>"><?= $d->department;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4"> 
                                	<div class="form-group">
                                        <label for="designation">Designation</label>
                                        <input type="text" name="designation" id="designation" class="form-control"  >
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-4">    
                                    <!--   ^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
                                     will support the following formats:
								    8880344456/+918880344456/+91 8880344456/
								    +91-8880344456/08880344456/918880344456  -->
                                     <div class="form-group">
                                        <label for="phone">Mobile</label><span class="text-danger">*</span>
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" value="+91" name="phone" id="phone" class="form-control" required="" placeholder="+91xxxxxxxxxx">
                                    </div> 
                                  </div> 
                            	</div>
                            	<div class="row">
	                                <div class="col-lg-4 col-md-4 col-sm-4"> 
	                                    <div class="form-group">
	                                        <label for="address">Address</label>
	                                        <textarea name="address" id="address" class="form-control"  ></textarea>
                                    	</div>
	                                </div>                                 
                            	</div>                            	
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>           
    </section>
</div>
<!-- /.content-wrapper -->
