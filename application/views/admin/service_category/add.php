<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Service Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Service Category</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Service/service_category_add" method="post">
                                    <div class="form-group">
                                       <label for="title">Office</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="officeId" required>
	                                	<option value="">Select</option>
	                                	<?php foreach($office as $o){ ?>
	                                	<option value="<?= $o->id; ?>"><?= $o->office;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Category</label><span class="text-danger">*</span>
                                        <input type="text" name="category" id="category" class="form-control"  required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
