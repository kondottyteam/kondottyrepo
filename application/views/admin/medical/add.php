<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Medical Equipments
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Medical Equipments</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Medical/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div> 
                                    <!--   ^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
                                     will support the following formats:
								    8880344456/+918880344456/+91 8880344456/
								    +91-8880344456/08880344456/918880344456  -->
                                     <div class="form-group">
                                        <label for="phone">Mobile</label><span class="text-danger">*</span>
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" value="+91" name="phone" id="phone" class="form-control" required="" placeholder="+91xxxxxxxxxx">
                                    </div>                                  
                                    <div class="form-group">
                                        <label for="photo">Medical Image (768x576)</label><span class="text-danger">*</span></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo" id="photo" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
