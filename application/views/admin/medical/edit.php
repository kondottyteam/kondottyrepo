<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Medical Equipments
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Medical Equipments</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Medical/edit" method="post"  enctype="multipart/form-data">
                                 <?php foreach($results as $r){  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                     <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control"  value="<?php echo $r['name'];?>">
                                    </div> 
                                    <!--   ^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
                                     will support the following formats:
								    8880344456/+918880344456/+91 8880344456/
								    +91-8880344456/08880344456/918880344456  -->
                                     <div class="form-group">
                                        <label for="phone">Mobile</label><span class="text-danger">*</span>
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$" name="phone" id="phone" class="form-control" required="" value="<?php echo $r['phone'];?>" placeholder="+91xxxxxxxxxx">
                                    </div>      
                                    <div class="form-group">
                                        <label for="description">Image want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage" id="ImageChange"  value="yes" onclick="imgDisply(this.value)">Y
                                       <input type="radio" name="changeImage" id="ImageNoChange"  value="no" onclick="imgDisply(this.value)" checked="">N
                                    </div>
                                    <div class="form-group" style="display: none" id="imageDivId">
                                        <label for="image">Medical Image (768x576)</label></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo" data-action="show_thumbnail">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('imageDivId').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('imageDivId').style.display='none';
	}
}
</script>
