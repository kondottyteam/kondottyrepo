<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Enquiry
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Enquiry</h3>
                       
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Aadhar No</th>
                                <th>Address</th>
                                <th>Description</th>
                                <th>Date</th> 
                                <th width="150px;">Action</th>                               
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	$i =$this->uri->segment(3); 
                                foreach ($records as $data) {
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $data->userName; ?></td>
                                        <td><?= $data->phone; ?></td>
                                        <td><?= $data->aadhar; ?></td>
                                        <td><?= $data->address; ?></td>
                                        <td><?= $data->description; ?></td>
                                        <td><?= $data->eDate; ?></td>      
                                         <td>                                           
                                            <a href="<?= site_url(); ?>/Enquiry/delete/<?= $data->id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                        </td>                                  
                                    </tr>
                                    <?php                                   
                                }
                            } else {
                                ?>
                                <tr><td colspan="6" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
               <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>