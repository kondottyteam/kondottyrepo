<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Maps
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Maps</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Maps/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="heading">Heading</label>
                                        <input type="text" name="heading" id="heading" class="form-control">
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="photo">Maps Image (768x576)</label><span class="text-danger">*</span></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo" id="photo" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
