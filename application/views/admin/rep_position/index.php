<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
          Chairman / Vice Chairman / Secretary
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Chairman</h3>
                    </div>
                    <div class="box-body">
                        <form name="president_form" action="<?php echo site_url(); ?>/rep_position/add" method="post"  enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="name2">Chairman</label><span class="text-danger">*</span>
                                        <?php if($president !=1){?>
                                        <select name="name1" id="name1" class="form-control" required>
                                            <?php foreach ($representatives as $rep_data) {
                                                ?>
                                                <option value="<?= $rep_data->id?>" > <?= $rep_data->name; ?> </option>
                                            <?php }?>
                                        </select>
                                       <?php }else { ?>
                                            <select name="name1" id="name1" class="form-control" required>
                                                <?php foreach ($representatives as $rep_data) {
                                                    ?>
                                                    <option value="<?= $rep_data->id?>" <?php if($rep_data->id==$presidentName[0]->id){ echo "selected"; }?> > <?= $rep_data->name; ?> </option>
                                                <?php }?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submitPresident">Update</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
          
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Vice Chairman</h3>
                    </div>
                    <div class="box-body">
                        <form name="vc_form" action="<?php echo site_url(); ?>/rep_position/add" method="post"  enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="name2">Vice Chairman</label><span class="text-danger">*</span>
                                        <?php if($vpresident !=1){?>
                                            <select name="name2" id="name2" class="form-control" required>
                                                <?php foreach ($representatives as $rep_data) {
                                                    ?>
                                                    <option value="<?= $rep_data->id?>" > <?= $rep_data->name; ?> </option>
                                                <?php }?>
                                            </select>
                                        <?php }else { ?>
                                            <select name="name2" id="name2" class="form-control" required>
                                                <?php foreach ($representatives as $rep_data) {
                                                    ?>
                                                    <option value="<?= $rep_data->id?>" <?php if($rep_data->id==$vcName[0]->id){ echo "selected"; }?> > <?= $rep_data->name; ?> </option>
                                                <?php }?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submitVc">Update</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add Secretary</h3>
                    </div>
                    <div class="box-body">
                    	<form action="<?php echo site_url(); ?>/rep_position/staffAdd" method="post"  enctype="multipart/form-data">
                    	<?php
                        foreach ($positions as $pos_data) {
                            if($pos_data->id=='3'){
                        ?>
                           <input type="hidden" name="positionId3" id="positionId3" value="<?php echo $pos_data->id; ?>">
                            <input type="hidden" name="position3" readonly="" id="position3" value="<?php echo $pos_data->position; ?>" >
                        <?php
                        }}
                        if(count($secretary>0))
                        {	
                        foreach($secretary as $sec) {
                        	 $staffName	= $sec->staffName;
                        	 $phone		= $sec->phone;
                        	 $address   = $sec->address; 
                        	 $photo     = $sec->photo;
                        	 }
                        }
                        else{
							$staffName	= '';
                        	$phone		= '';
                        	$address   = ''; 
						}
                        ?>
                    	<div class="row">
                        	<div class="col-lg-4 col-md-4 col-sm-4">
	                          	<div class="form-group">
	                                <label for="name2">Secretary</label><span class="text-danger">*</span>
	                                <input type="text" name="staffName" id="staffName" class="form-control" required value="<?php echo @$staffName  ?>">
	                            </div>
	                        </div>	                        
                        	<div class="col-lg-4 col-md-4 col-sm-4">
	                          	<!--   ^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
                                     will support the following formats:
								    8880344456/+918880344456/+91 8880344456/
								    +91-8880344456/08880344456/918880344456  -->
                                     <div class="form-group">
                                        <label for="phone">Mobile</label><span class="text-danger">*</span>
                                        <input type="tel" pattern="^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$"  name="phone" id="phone" class="form-control" required="" placeholder="+91xxxxxxxxxx" value="<?php echo @$phone  ?>">
                                    </div> 
	                        </div>
	                        <div class="col-lg-4 col-md-4 col-sm-4">
	                          	<div class="form-group">
	                                <label for="address">Address</label><span class="text-danger">*</span>
	                                <textarea name="address" id="address" class="form-control" ><?php echo @$address  ?></textarea>
	                            </div>
	                        </div>	                        
	                    </div>
	                    <div class="row">
                        	<div class="col-lg-4 col-md-4 col-sm-4">
	                            <div class="form-group">
                                        <label for="photo">Uploade Photo </label>
                                        <input type="file" name="photo" data-action="show_thumbnail">
                                </div>
	                        </div>
	                        <div class="col-lg-4 col-md-4 col-sm-4">
	                        <?php
	                        if(@$photo)
	                        {	                        	
	                        ?>
	                            <div class="form-group">
                                    <img src="<?php echo base_url().$photo; ?>" height="30%" width="30%">
                                </div>
                            <?php
                            }
                            ?>
	                        </div>
                        	<div class="col-lg-4 col-md-4 col-sm-4">
	                          	<div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat" name="submit2">Update</button>
                                </div>
	                        </div>
	                    </div>
                                
                        </form>                            
                    </div>
                </div>
          
            </div>
        </div>
    </section>
</div>
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}



</script>
<!-- /.content-wrapper -->
