<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Hotspot
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Hotspot</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <form action="<?= site_url(); ?>/Hotspot/add" method="post"  enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="heading">Heading</label><span class="text-danger">*</span>
                                        <input type="text" name="heading" id="heading" class="form-control" required>
                                    </div> 
                                </div> 
	                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"></textarea>
                                    </div> 
                                </div> 
                            </div>
                            <div class="row">
	                            <div class="col-lg-4 col-md-4 col-sm-4">  
                                    <div class="form-group">
                                        <label for="photo">Hotspot Image 1 (768 * 432)
                                        </label><span class="text-danger">*</span></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo" id="photo"  required>
                                    </div>
                                 </div> 
	                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="photo">Hotspot Image 2 (768 * 432)</label></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo2" id="photo2">
                                    </div>
                                 </div> 
	                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="photo">Hotspot Image 3 (768 * 432)</label></br>
                                        		[Upload only 'jpg | jpeg | png | gif']</br>
                                        <input type="file" name="photo3" id="photo3">
                                    </div>
                                 </div> 
	                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                 </div> 
	                          </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
