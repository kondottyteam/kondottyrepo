<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Panchayath - Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url(); ?>css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/iCheck/flat/blue.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= base_url(); ?>js/jquery-2.2.4.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
    	<header class="main-header">
        <!-- Logo -->
        <a href="<?= site_url(); ?>/dashboard" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Kondotty Panchayath</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span><?= $_SESSION['username']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= base_url(); ?>index.php/Login/changePasswordForm" class="btn btn-flat btn-default">Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= base_url(); ?>index.php/logout" class="btn btn-flat btn-default">
                                        <span class="hidden-xs">Logout</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= base_url(); ?>img/default_user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?= $_SESSION['username']; ?></p>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li>
	                <a href="<?= site_url(); ?>/dashboard">
	                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/EmergencyContacts/index">
	                    <i class="fa fa-user"></i> <span>Emergency contacts</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Gallery/index">
	                    <i class="fa fa-image"></i> <span>Gallery</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Hotspot/index">
	                    <i class="fa fa-star"></i> <span>Hotspot</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/News/index">
	                    <i class="fa fa-newspaper-o"></i> <span>News</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Flash_news/index">
	                    <i class="fa fa-ellipsis-h"></i> <span>Flash News</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Enquiry/index">
	                    <i class="fa fa-commenting-o"></i> <span>Enquiry</span>
	                </a>
                </li>
                <li>
	    	        <a href="<?= base_url(); ?>index.php/representatives/index">
	    	           <i class="fa fa-user"></i> <span>Representative</span>
	    	        </a>
	        	</li>
               <!--<li>
                <a href="<?= base_url(); ?>index.php/position/index">
                    <i class="fa fa-street-view"></i> <span>Position</span>
                </a>
                </li>-->
                <li>
	                <a href="<?= base_url(); ?>index.php/rep_position/index">
	                     <i class="fa fa-users"></i> <span>Chairman/Vice Chairman</span>
	                </a>
                </li>
                
                <li>
	                <a href="<?= site_url(); ?>/Committee/index">
	                    <i class="fa fa-users"></i> <span>Standing Committee</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/CommitteeDetails/index">
	                    <i class="fa fa-file-text"></i> <span>Standing Committee Details</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Plan_category/index">
	                    <i class="fa fa-cog"></i> <span>Plan Category</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Plan_details/index">
	                    <i class="fa fa-tasks"></i> <span>Plan Details</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Department/index">
	                    <i class="fa fa-cog"></i> <span>Department</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Member/index">
	                    <i class="fa fa-cog"></i> <span>Staff</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Service/service_office_index">
	                    <i class="fa fa-bank"></i> <span>Service Office</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Service/service_category_index">
	                    <i class="fa fa-cog"></i> <span>Service Category</span>
	                </a>
                </li>
                <li>
	                <a href="<?= site_url(); ?>/Service/service_index">
	                    <i class="fa fa-tasks"></i> <span>Services</span>
	                </a>
                </li>
                <!--<li>
	                <a href="<?= site_url(); ?>/Medical/index">
	                    <i class="fa fa-medkit"></i> <span>Medical Equipments</span>
	                </a>
                </li>                
                <li>
	                <a href="<?= site_url(); ?>/Pmc/index">
	                    <i class="fa fa-user"></i> <span>PMC</span>
	                </a>
                </li>-->
                <li>
	                <a href="<?= site_url(); ?>/Ad/index">
	                    <i class="fa fa-tv"></i> <span>Ad</span>
	                </a>
                </li>
                <!--<li>
	                <a href="<?= site_url(); ?>/Maps/index">
	                    <i class="fa fa-map-marker"></i> <span>Maps</span>
	                </a>
                </li>-->
                
                <!--<li>
                <a href="<?= site_url(); ?>/Paliative/index">
                    <i class="fa fa-ambulance"></i> <span>Aswas Pain And Paliative care</span>
                </a>
                </li>-->
                
                
                
                <!--<li>
	                <a href="<?= site_url(); ?>/Statistics/index">
	                    <i class="fa fa-industry"></i> <span>Statistics</span>
	                </a>
                </li>-->
                
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>